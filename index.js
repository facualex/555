const r1Input = document.querySelector('#r1ValueInput');
const r2Input = document.querySelector('#r2ValueInput');
const cInput = document.querySelector('#cValueInput');

const r1 = document.querySelector('#r1Value');
const r2 = document.querySelector('#r2Value');
const c = document.querySelector('#cValue');

const frecValue = document.querySelector('#frecValue');

r1Input.addEventListener('input', inputChange);
r2Input.addEventListener('input', inputChange);
cInput.addEventListener('input', inputChange);

function inputChange(event) {
  event.target.nextElementSibling.innerHTML = event.target.value;
  updateFrecuencia();
}

function updateFrecuencia() {
  frecValue.innerHTML =
    1.44 /
      ((parseInt(r1Input.value) + 2 * parseInt(r2Input.value)) *
        (parseInt(cInput.value) * Math.pow(10, -6))) +
    ' Hz';
}
